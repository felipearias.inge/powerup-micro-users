INSERT INTO `user` (
    `id`,
    `address`,
    `dni_number`,
    `id_dni_type`,
    `id_person_type`,
    `mail`,
    `name`,
    `password`,
    `phone`,
    `surname`,
    `token_password`
  )
VALUES
  (
    '1',
    'st 123 # 456',
    '123',
    '1',
    '1',
    'email@some.com',
    'Name',
    '$2a$10$GlsGSNhkbVon6ZOSNMptOu5RikedRzlCAhMa7YpwvUSS0c88WT99S',
    '1234567890',
    'Surname',
    NULL
  );


INSERT INTO `user` (`id`,`address`,`birth_date`,`dni_number`,`mail`,`name`,`password`,`phone`,`surname`,`token_password`,`id_role`) VALUES (1,'admin','2023-05-18 19:00:00.000000','1111','admin@gmail.com','admin','$2a$10$0Rrh1dYZ0z2dSXpeED7DCOImUURkFrAN1hownpg3QGIoEp89DmTti','+571245785421','admin',NULL,1);
INSERT INTO `user` (`id`,`address`,`birth_date`,`dni_number`,`mail`,`name`,`password`,`phone`,`surname`,`token_password`,`id_role`) VALUES (2,'user','2023-05-22 19:00:00.000000','2322','user@gmail.com','user','$2a$10$7xJf5y8AhNUCvC83xpiuFOX4McTqyrJwzxtJk7iFgBF0P.0obrsHa','+573547865411','user2',NULL,2);
INSERT INTO `user` (`id`,`address`,`birth_date`,`dni_number`,`mail`,`name`,`password`,`phone`,`surname`,`token_password`,`id_role`) VALUES (3,'owner','2000-05-27 19:00:00.000000','333','owner@gmail.com','owner','$2a$10$MB7OXvmgudmSg27kuJ2qxOC3aRx9q/Bjc/KKJtnHjG7/mP/MyZGNq','+573547865412','owner',NULL,3);
INSERT INTO `user` (`id`,`address`,`birth_date`,`dni_number`,`mail`,`name`,`password`,`phone`,`surname`,`token_password`,`id_role`) VALUES (4,'employe','1899-08-08 19:00:00.000000','4','employe@gmail.com','employe','$2a$10$XtqKpON/AwTK1qJd8Ahj4On/bi9cP8svk6dCdqCHd.Lnx8u1ptd9u','+573547865414','employe',NULL,4);


INSERT INTO `role` (`id`,`description`,`name`) VALUES (1,'ROLE_ADMIN','ROLE_ADMIN');
INSERT INTO `role` (`id`,`description`,`name`) VALUES (2,'ROLE_USER','ROLE_USER');
INSERT INTO `role` (`id`,`description`,`name`) VALUES (3,'ROLE_OWNER','ROLE_OWNER');
INSERT INTO `role` (`id`,`description`,`name`) VALUES (4,'ROLE_EMPLOYEE','ROLE_EMPLOYEE');
INSERT INTO `role` (`id`,`description`,`name`) VALUES (5,'ROLE_CUSTOMER','ROLE_CUSTOMER');
--si no funciona procura usar quitar el ID de los insert!!

