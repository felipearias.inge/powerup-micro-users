package com.pragma.powerup.usermicroservice.domain.repositories;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface IUserRepository extends JpaRepository<UserEntity, Long> {

    @Query("SELECT u FROM UserEntity u WHERE u.id = ?1")
    Optional<UserEntity> findById(String id);

    Optional<UserEntity> findByDniNumber(String dniNumber);

    Boolean existsByDniNumber(String dniNumber);

    @Query("SELECT u FROM UserEntity u WHERE u.mail = ?1")
    Optional<UserEntity> findByMail(String mail);


    boolean existsByMail(String mail);

    @Query("SELECT u.phone FROM UserEntity u WHERE u.id = :id")
    String findPhoneById(Long id);//trae el telefono
    @Query("SELECT u.idRole.id FROM UserEntity u WHERE u.id = :id")
    String findIdRoleById(Long id);//Trae el role

    @Query("SELECT u.id FROM UserEntity u WHERE u.mail = :mail")
    Long findIdByEmail(String mail);


}
