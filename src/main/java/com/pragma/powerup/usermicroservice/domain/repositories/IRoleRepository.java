package com.pragma.powerup.usermicroservice.domain.repositories;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface IRoleRepository extends JpaRepository<RoleEntity, Long> {


    Optional<RoleEntity> findById(String id);

    @Query("SELECT u.description FROM RoleEntity u WHERE u.id = ?1")
    String findDescriptionById(long id);



}
