package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.impl;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.UserEntity;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.UserRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IUserHandler;
import com.pragma.powerup.usermicroservice.adapters.driving.http.mapper.IUserRequestMapper;
import com.pragma.powerup.usermicroservice.domain.api.IRoleServicePort;
import com.pragma.powerup.usermicroservice.domain.api.IUserServicePort;
import com.pragma.powerup.usermicroservice.domain.repositories.IUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import static org.springframework.data.jpa.domain.AbstractPersistable_.id;

@Service
@RequiredArgsConstructor
public class UserHandlerImpl implements IUserHandler {
    private final IUserServicePort personServicePort;
    private final IUserRequestMapper personRequestMapper;
    private final IRoleServicePort roleServicePort;
    private final IUserRepository userRepository;


    @Override
    public void saveUser(UserRequestDto userRequestDto) {

        var authentication = SecurityContextHolder.getContext().getAuthentication();
        var iterator = authentication.getAuthorities().iterator();
        String roleUser = "";
        while (iterator.hasNext()) {
            roleUser = String.valueOf(iterator.next());
        }

        Long idRole =userRequestDto.getIdRole().getId();
        String description = roleServicePort.findDescriptionById(idRole);
        System.out.println(description);

        if (roleUser.equals("ROLE_OWNER") && description.equals("ROLE_EMPLOYEE")) {//hu_06: Crear cuenta empleado
            personServicePort.saveUser(personRequestMapper.toUser(userRequestDto));
        }else if (roleUser.equals("ROLE_ADMIN") && description.equals("ROLE_OWNER")) {
            personServicePort.saveUser(personRequestMapper.toUser(userRequestDto));
        }
        else if (roleUser.equals("ROLE_USER") || roleUser.equals("ROLE_EMPLOYEE")) {
            throw new RuntimeException("Unauthorized access. Only the owner can perform this action.");
        }

        else {

            throw new RuntimeException("Unauthorized access. This role can't perform this action.");

        }
    }

    @Override
    public UserEntity findUserById(String id) {
        return personServicePort.findUserById(id);
    }

    @Override
    public void saveClient(UserRequestDto userRequestDto) {
        long idRole = userRequestDto.getIdRole().getId();
        String description = roleServicePort.findDescriptionById(idRole);
        System.out.println("el id es" +description );
        if ( description.equals("ROLE_CUSTOMER")) { //IDENTIFICANDO EMPLEADO
            personServicePort.saveUser(personRequestMapper.toUser(userRequestDto));
        }
        else {

            throw new RuntimeException("Unauthorized access. This role can't perform this action.");

        }
    }

    @Override
    public String findPhoneById(Long id) {
        String phone = null;
        String idRole = userRepository.findIdRoleById(id);
        String description = roleServicePort.findDescriptionById(Long.valueOf(idRole));
        System.out.println(description);
        if (!description.equals("ROLE_CUSTOMER")) {
            throw new RuntimeException("Unauthorized access. This role can't perform this action.");
        }
        return userRepository.findPhoneById(id);
    }

    public Long findUserIdByEmail(String mail) {
        return userRepository.findIdByEmail(mail);
    }
}
