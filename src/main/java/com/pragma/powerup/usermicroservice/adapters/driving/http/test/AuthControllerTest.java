package com.pragma.powerup.usermicroservice.adapters.driving.http.test;

import com.pragma.powerup.usermicroservice.adapters.driving.http.controller.AuthController;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.LoginRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.JwtResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.impl.AuthHandlerImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AuthControllerTest {

    @InjectMocks
    private AuthController authController;

    @Mock//comportamiuentop de la prueba, mira el comportamiento de los objetos de la clase
    private AuthHandlerImpl authHandler;

    @Test//Aqui se basa la prueba
    public void testLogin(){
        LoginRequestDto loginRequestDto = new LoginRequestDto("usuario", "contraseña");
        JwtResponseDto jwtResponseDto = new JwtResponseDto("token");

        when(authHandler.login(loginRequestDto)).thenReturn(jwtResponseDto);

        ResponseEntity<JwtResponseDto> response = authController.login(loginRequestDto);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(jwtResponseDto, response.getBody());
    }
}
