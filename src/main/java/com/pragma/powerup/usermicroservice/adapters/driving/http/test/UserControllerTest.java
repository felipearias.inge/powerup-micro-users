package com.pragma.powerup.usermicroservice.adapters.driving.http.test;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RoleEntity;
import com.pragma.powerup.usermicroservice.adapters.driving.http.controller.UserRestController;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.UserRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IUserHandler;
import com.pragma.powerup.usermicroservice.configuration.Constants;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Date;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {

    @InjectMocks
    private UserRestController userRestController;

    @Mock
    private IUserHandler iUserHandler;

    @Test
    public void testSaveUser() {
        UserRequestDto userRequestDto = new UserRequestDto(
                "Federico",
                "Aponte",
                "Fede@gmail.com ",
                "+573154538880",
                new Date(),
                "calle quinta porra",
                "11",
                "11",
                new RoleEntity(1L, "", "")

        );

        ResponseEntity<Map<String, String>> response = userRestController.savePerson(userRequestDto);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(Constants.PERSON_CREATED_MESSAGE, response.getBody().get(Constants.RESPONSE_MESSAGE_KEY));

    }
}
