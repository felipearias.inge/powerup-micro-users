package com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RoleEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;

@AllArgsConstructor
@Getter
public class UserRequestDto {
    private String name;
    private String surname;
    private String mail;
    private String phone;
    private Date birthDate;
    private String address;
    private String dniNumber;
    private String password;
    private RoleEntity idRole;//este se muestra en el swagger,(trasferencias y recepcion)
}
