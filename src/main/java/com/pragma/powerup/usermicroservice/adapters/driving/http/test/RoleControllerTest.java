package com.pragma.powerup.usermicroservice.adapters.driving.http.test;

import com.pragma.powerup.usermicroservice.adapters.driving.http.controller.RoleRestController;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.RoleResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IRoleHandler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RoleControllerTest {

    @Mock
    private IRoleHandler roleHandler;

    @InjectMocks
    private RoleRestController roleRestController;

    @Test
    public void testGetAllRoles() {
        RoleResponseDto role1 = new RoleResponseDto("role1","");
        RoleResponseDto role2 = new RoleResponseDto("role2","");
        List<RoleResponseDto> mockRoles = Arrays.asList(role1, role2);

        when(roleHandler.getAllRoles()).thenReturn(mockRoles);


        ResponseEntity<List<RoleResponseDto>> response = roleRestController.getAllRoles();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(mockRoles, response.getBody());
    }
}