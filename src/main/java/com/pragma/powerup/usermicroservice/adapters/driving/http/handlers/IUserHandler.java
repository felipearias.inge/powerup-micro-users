package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.UserEntity;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.UserRequestDto;

public interface IUserHandler {
    void saveUser(UserRequestDto personRequestDto);

    UserEntity findUserById(String id);

    void saveClient(UserRequestDto personRequestDto);

     String findPhoneById(Long id);

    Long findUserIdByEmail(String mail);
}
