package com.pragma.powerup.usermicroservice.adapters.driving.http.controller;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.RoleResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IRoleHandler;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.impl.RoleHandlerImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RoleRestControllerTest {
    @Mock
    private IRoleHandler roleHandlerImpl;//simule los objetos que estan dentro del IROLEHANDLER

    @InjectMocks
    private RoleRestController roleRestController;//Se injecta los Mocks al controlar

    @BeforeEach
    void setUp(){

        roleHandlerImpl = mock(RoleHandlerImpl.class);//platohandlerimpl va a tener un mock
        roleRestController = new RoleRestController(roleHandlerImpl);//resive los objetos del roleHandlerImpl
    }

    @Test
    public void testGetAllRoles() {
        RoleResponseDto role1 = new RoleResponseDto("role1","");
        RoleResponseDto role2 = new RoleResponseDto("role2","");
        List<RoleResponseDto> mockRoles = Arrays.asList(role1, role2);

        when(roleHandlerImpl.getAllRoles()).thenReturn(mockRoles);


        ResponseEntity<List<RoleResponseDto>> response = roleRestController.getAllRoles();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(mockRoles, response.getBody());
    }
}
