package com.pragma.powerup.usermicroservice.adapters.driving.http.controller;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RoleEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.UserEntity;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.UserRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IUserHandler;
import com.pragma.powerup.usermicroservice.configuration.Constants;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Date;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserRestControllerTest {
    @Mock
    private IUserHandler userHandlerImpl;

    @InjectMocks
    private UserRestController userRestController;
    @BeforeEach
    void setUp(){

        userHandlerImpl = mock(IUserHandler.class);
        userRestController = new UserRestController(userHandlerImpl);
    }


    @Test
    public void testSavePerson() {
        UserRequestDto userRequestDto = new UserRequestDto(
                "Doe",
                "john",
                "john.doe@example.com",
                "123456789",
                new Date(),
                "1234567890",
                "secretpassword",
                "1234",
                                new RoleEntity(1L, "", "")
        );

        ResponseEntity<Map<String, String>> response = userRestController.savePerson(userRequestDto);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(Constants.PERSON_CREATED_MESSAGE, response.getBody().get(Constants.RESPONSE_MESSAGE_KEY));
    }
    @Test
    void testFindUserById() {
        // Configuración del mock
        UserEntity mockUser = new UserEntity();
        mockUser.setId(Long.valueOf("1"));
        mockUser.setName("John Doe");
        when(userHandlerImpl.findUserById(anyString())).thenReturn(mockUser);

        // Llamada al método bajo prueba
        ResponseEntity<UserEntity> response = userRestController.findUserById("1");

        // Verificación de la respuesta
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(mockUser, response.getBody());
    }

    @Test
    void testSaveCliente(){
        UserRequestDto userRequestDto = new UserRequestDto(
                "Doe",
                "john",
                "john.doe@example.com",
                "123456789",
                new Date(),
                "1234567890",
                "secretpassword",
                "1234",
                new RoleEntity(1L, "", "")
        );

        ResponseEntity<Map<String, String>> response = userRestController.saveClient(userRequestDto);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(Constants.PERSON_CREATED_MESSAGE, response.getBody().get(Constants.RESPONSE_MESSAGE_KEY));
    }
}